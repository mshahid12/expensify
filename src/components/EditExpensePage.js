import React from 'react';

const EditExpensePage = (props) => {
    console.log(props);
    return (
        <div>
            <p>We are accessing edit page with id {props.match.params.id}</p>
        </div>
    );
}

export default EditExpensePage;