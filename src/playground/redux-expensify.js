import { createStore, combineReducers } from 'redux';
import uuid from 'uuid'

//Add expense

const addExpense = (
    {description = '', note ='', amount = 0, createdAt = 0} = {}
) => {
    return {
        type: 'ADD_EXPENSE',
        expense : {
            id : uuid(),
            description,
            note,
            amount,
            createdAt
        }
    };
}

//remove expense
//edit expense
//set text filter
//sort by date
//sort by amount
//set start date
//set end date

//expenses reducer 

const expensesReducerDefaultState = [];

const expensesReducer = (state = expensesReducerDefaultState, action) => {
    switch (action.type) {
        case 'ADD_EXPENSE':
            return [
                ...state,
                action.expense
            ];
        default: return state;
    }
}

//filters reducer 

const filtersReducersDefaultState= {
    text : '',
    sortBy: 'date',
    startDate : undefined,
    endDate : undefined
};

const filtersReducer = (state = filtersReducersDefaultState, action) => {
    switch(action.type){
        default :
            return state;
    }
}


//store creation

const store = createStore(
    combineReducers({
        expenses : expensesReducer,
        filters : filtersReducer
    })
);

store.subscribe(() => {
    console.log(store.getState());
});

store.dispatch(addExpense({
    description : 'rent', amount : 100
}));

store.dispatch(addExpense({
    description : 'coffee', amount : 300
}));


const demoState = {
    expenses: [{
        id: 'qweqweqw',
        description: 'january rent',
        note: 'this was the final payment for that address',
        amount: 54500,
        createdAt: 0
    }],
    filters: {
        text: 'rent',
        sortBy: 'amount', //date or amount
        startDate: undefined,
        endDate: undefined
    }
};