import {
    createStore
} from 'redux';

//Action generators
const incrementCount = ({incrementBy = 1} = {}) => {
    return {
        type : 'INCREMENT',
        incrementBy 
    };
}

const decrementCount = ({decrementBy = 1} = {}) => {
    return {
        type : 'DECREMENT',
        decrementBy
    };
}

const resetCount = ({count = 0} = {}) => {
    return {
        type : 'RESET',
        count
    };
}

const setCount = ({count} = {}) => {
    return {
        type : 'SET',
        count
    };
}

//Reducers
//Reducers are pure functions means they only use internal variable and not anything from outside
//Never change state or action

const countReducer = (state = { count: 0 }, action) => {
    switch (action.type) {
        case 'INCREMENT':
            const incrementBy = typeof action.incrementBy === 'number' ? action.incrementBy : 1;
            return {
                count: state.count + incrementBy
            };
        case 'DECREMENT':
            const decrementBy = typeof action.decrementBy === 'number' ? action.decrementBy : 1;
            return {
                count: state.count - decrementBy
            };
        case 'RESET':
            return {
                count: 0
            };
        case 'SET':
            return {
                count: action.count
            };
        default:
            return state;
    }
};

const store = createStore(countReducer);

const unsubscribe = store.subscribe(() => {
    console.log(store.getState());
});

store.dispatch(incrementCount({ incrementBy : 100 }));

store.dispatch(decrementCount({decrementBy : 10}));

store.dispatch(resetCount({count:0}));

store.dispatch(setCount({count : 500}));